# LIS3784 - INTERMEDIATE DATABASE MANAGEMENT

## Mary Marguerite Meberg

### LIS3784 Requirements:

*Course Work Links:*

1. [A1 README.md](https://bitbucket.org/mmm18m/lis4368/src/master/a1/README.md)
	* Install JDK
* Install Tomcat
* Provide screenshots of installations
* Create Bitbucket repo
* Complete Bitbucket tutorials
    * Provide git command descriptions

2. [A2 README.md](https://bitbucket.org/mmm18m/lis4368/src/master/a2/README.md)
	* 

3. [A3 README.md](https://bitbucket.org/mmm18m/lis4368/src/master/a3/README.md)
    * 

4. [A4 README.md](https://bitbucket.org/mmm18m/lis4368/src/master/a4/README.md)
	* 

  
5. [A5 README.md](https://bitbucket.org/mmm18m/lis4368/src/master/a5/README.md)
	* 

 
6. [P1 README.md](https://bitbucket.org/mmm18m/lis4368/src/master/p1/README.md)
	* 


7. [P2 README.md](https://bitbucket.org/mmm18m/lis4368/src/master/p2/README.md)
	* 