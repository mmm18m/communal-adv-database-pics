I am the bone of my sword
Steel is my body
Fire is my blood
I have created over a thousand blades
Unknown to death
Nor known to life
Have withstood pain to create many weapons
Yet, those hands will never hold anything
So, as I pray
Unlimited Blade Works